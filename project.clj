(defproject canvas "0.5.0-SNAPSHOT"
  :description "Clojurescript wrapper to js/canvas"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.8.0"]]
  :plugins 
  [[lein-cljsbuild "1.1.6"]
   [lein-doo "0.1.7"]]
  :cljsbuild
  {:builds
   {:minify
    {:source-paths ["src"]
     :compiler {:optimizations :advanced}
     :pretty-print false}}
   {:dev
    {:source-paths ["src"]
     :compiler {:optimizations :whitespace}}}
   {:test
    {:id "test"
     :source-paths ["src" "test"]
     :compiler
     {:output-to "target/canvas/tests.js"
      :output-dir "target"
      :main citrus.core-test
      :optimizations :none
      :target :nodejs}}}})
