# Canvas

Clojurescript wrapper to js/canvas

## Usage

```shell
git clone git@gitlab.com:Drake.aren/canvas.git
cd canvas
lein install
```

then add this to `project.clj` into `:dependencies` in your project

```properties 
[canvas "0.5.0-SNAPSHOT"]
```
